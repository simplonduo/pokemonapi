

from fastapi import FastAPI
from pydantic.main import BaseModel

app = FastAPI()


#schema
class Chasseur(BaseModel):
    nom: str
    age: int
    id: int

class ChasseurSansID(BaseModel):
    nom: str
    age: int


chasseurs=[]

@app.post('/chasseur')
def add_chasseur(nouveau_chasseur: ChasseurSansID):
    chasseur_normal = Chasseur(
        nom = nouveau_chasseur.nom,
        age = nouveau_chasseur.age,
        id = len(chasseurs)
    )
    chasseurs.append(chasseur_normal)
    return chasseur_normal


@app.get("/chasseurs")
def get_all_chasseurs():
    return chasseurs